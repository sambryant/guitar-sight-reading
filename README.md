# Guitar Sight Reading Generator

Simple python program that generates random sheet music for use in training sightreading for classical guitar.

It can generate exercises of multiple difficulties in any key in position 1, 3, 5, and 8. It's meant to help train memorization of fingerings and not more sophisticated elements of sightreading like technique, expression, phrasing, etc.

🌞 do not expect these pieces to sound nice 🌞

To get started, simply download the repository:

        git clone git@gitlab.com:sambryant/guitar-sight-reading.git

And generate some sheet music:

        cd guitar-sight-reading
        python main.py 0                # C major

This will generate (and open) an HTML file with randomly generated exercises in the key of C major at several positions. It can also produce music in other keys and modes:

        python main.py 2 --mode major   # D major
        python main.py 4 --mode minor   # E minor
        python main.py 5 --mode minor   # F minor

To see all options, try `python main.py -h`

## Examples:

1st position key of C - easy sequence

![c-major-p1-easy](examples/c-major-p1-easy.svg)

5th position key of A - medium sequence

![a-major-p5-standard](examples/a-major-p5-standard.svg)

8th position key of E minor - difficult sequence

![e-minor-p8-difficult](examples/e-minor-p8-difficult.svg)

## About:

This is a quick program I wrote in a day or so.
The music is generated using a basic algorithm which favors melodic lines with smaller intervals, but can be configured by modifying `sequence.json`.
The sheet music is written as an SVG image using simple graphical primitives. You can change the appearance by tweaking the variables in `graphics.json`.

I have no plans to do anything with this or turn it in a web app. You are free to do whatever you'd like with it.

## To Do:

Goals:
 - ~~Accept command line args~~
 - ~~Ability to configure sequence generation parameters via JSON~~
 - ~~Common chord and interval patterns~~
 - Fix key signatures with flats (e.g. key sig for F major is wrong)
 - More sophisticated sequence generation algorithm
 - Finger labels next to notes
 - Counterpoint???
