from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List, Set, Tuple
import numpy as np

from scale import *
from tone import *
from sequence import *
from writer import *

class GraphicSettings():
  measures_per_line: int = 4
  measure_width = 300
  measure_height = 64
  measure_sep_width = 3
  measure_sep_offset_y = 2
  notes_per_measure = 4
  header_padding = 100
  footer_padding = 100
  staff_padding_x = 100
  staff_padding_y = 75
  staff_line_width = 1
  key_padding_x = 100
  note_radius = 8
  note_tail_length_factor = 2.5 # number of lines note tails take up
  note_tail_width = 2
  sharp_height_factor = 1.3 # number of lines sharp signs occupy
  flat_width_factor = 0.8 # number of lines sharp signs occupy
  flat_height_factor = 1.3 # number of lines sharp signs occupy

  # Derived
  notes_per_line: int

  staff_height: float
  staff_width: float

  x_offset: float # x position of top-left corner of first measure
  y_offset: float # y position of top-left corner of first measure

  note_spacing_x: float
  note_spacing_y: float

  note_tail_length: float
  key_sig_spacing_x: float

  def __init__(self, **kwargs):
    for key, value in kwargs.items():
      setattr(self, key, value)

    self.notes_per_line = self.notes_per_measure * self.measures_per_line

    self.staff_height = self.measure_height + 2 * self.staff_padding_y
    self.y_offset = self.staff_padding_y + self.header_padding

    self.staff_width = self.measures_per_line * self.measure_width + self.key_padding_x
    self.x_offset = self.staff_padding_x + self.key_padding_x

    self.note_spacing_x = self.measure_width / self.notes_per_measure
    self.note_spacing_y = self.measure_height / 4
    self.note_tail_length = self.note_tail_length_factor * self.note_spacing_y
    self.key_sig_spacing_x = self.key_padding_x / 6

  @classmethod
  def load(cls, file='graphics.json'):
    kwargs = json.loads(open(file, 'r').read())
    return cls(**kwargs)

  def get_key_sig_x(self, sharp_index) -> x:
    return self.staff_padding_x + self.key_sig_spacing_x * (SHARP_INDEX_ORDERS[sharp_index] + 0.5)

  def get_measure(self, note_index) -> int:
    return int((note_index % self.notes_per_line) / self.notes_per_measure)

  def get_measure_x(self, measure_index: int) -> int:
    return self.x_offset + measure_index * self.measure_width

  def get_staff(self, note_index) -> int:
    return int(note_index / self.notes_per_line)

  def get_staff_y(self, staff_index: int) -> int:
    return self.y_offset + staff_index * self.staff_height

  def get_index_in_measure(self, note_index) -> int:
    return note_index % self.notes_per_measure

  def get_dimensions(self, num_notes) -> Tuple[int, int]:
    width = self.staff_width + 2 * self.staff_padding_x
    height = self.get_num_staffs(num_notes) * self.staff_height + self.header_padding + self.footer_padding
    return [width, height]

  def get_note_x(self, note_index: int) -> float:
    mi = self.get_measure(note_index)
    i = self.get_index_in_measure(note_index)
    return self.get_measure_x(mi) + self.note_spacing_x * (i + 0.5)

  def get_line_y(self, staff_index: int, line_index: int) -> float:
    return self.get_staff_y(staff_index) + 0.5 * line_index * self.note_spacing_y

  def get_num_staffs(self, num_notes) -> int:
    return self.get_staff(num_notes - 1) + 1

SHARP_INDEX_ORDERS = [0, 3, 1, 4, 2, 5]

class Graphics():
  tones: List[List[Tone]]
  p: GraphicSettings
  width: int
  height: int

  def __init__(self, settings: GraphicSettings, scale: Scale, tones):
    # Accept either list of tones or list of list of tones
    if isinstance(tones[0], Tone):
      self.tones = [[t] for t in tones]
    else:
      self.tones = tones
    self.p = settings
    self.scale = scale

    p = self.p
    self.width, self.height = p.get_dimensions(len(tones))

  def write(self, w: Writer):
    w.writeline('<svg xmlns="http://www.w3.org/2000/svg" height="%.2fpx" viewBox="0 0 %.2f %.2f" width="%.2fpx" fill="#000000">' % (self.height, self.width, self.height, self.width))
    w.writeline('  <path fill="white" d="M 0,0 0,%.2f %.2f,%.2f %.2f,0 0,0 z"/>' % (self.height, self.width, self.height, self.width))

    for index in range(self.p.get_num_staffs(len(self.tones))):
      SvgStaff(self.p, index, self.scale).write(w)

    for i, tones in enumerate(self.tones):
      for tone in tones:
        SvgNote(self.p, self.scale, i, tone).write(w)

    w.writeline('</svg>')

class SvgObject(ABC):

  @abstractmethod
  def write(self, w: Writer):
    pass

class SvgObjects(SvgObject):
  objects: List[SvgObject]

  def __init__(self):
    self.objects = []

  def write(self, w: Writer):
    for obj in self.objects:
      obj.write(w)

  def add(self, obj: SvgObject):
    self.objects.append(obj)

class SvgLine(SvgObject):

  def __init__(self, p1: Tuple(float, float), p2: Tuple(float, float), width: float = 1.0):
    self.p1 = p1
    self.p2 = p2
    self.width = width

  def write(self, w: Writer):
    w.writeline('  <path fill="none" style="stroke: black; stroke-width:%d;"' % self.width)
    w.writeline('        d="M %.2f,%.2f %.2f,%.2f z"/>' % (self.p1[0], self.p1[1], self.p2[0], self.p2[1]))

class SvgCirc(SvgObject):

  def __init__(self, c: Tuple(float, float), radius: float):
    self.c = c
    self.radius = radius

  def write(self, w: Writer):
    w.writeline('  <circle fill="black" style="stroke-width:0;" cx="%.2f" cy="%.2f" r="%.2f"/>' % (self.c[0], self.c[1], self.radius))

class SvgSharp(SvgObjects):

  def __init__(self, p: GraphicSettings, c: Tuple(float, float)):
    super().__init__()
    w = p.sharp_height_factor * p.note_spacing_y
    x, y = c
    d1, d2, d3, d4 = [-w/2, -0.21*w, 0.21*w, w/2]

    dy = 0.1745 * w / 2
    self.add(SvgLine((x + d1, y + d2 + dy), (x + d4, y + d2 - dy), width=2))
    self.add(SvgLine((x + d1, y + d3 + dy), (x + d4, y + d3 - dy), width=2))
    self.add(SvgLine((x + d2, y + d1), (x + d2, y + d4), width=2))
    self.add(SvgLine((x + d3, y + d1), (x + d3, y + d4), width=2))

_FLAT_SYMBOL = open('assets/flat.svg-part', 'r').read()

class SvgFlat2(SvgObject):

  def __init__(self, p: GraphicSettings, x: float, y: float):
    self.p = p
    self.x = x
    self.y = y

  def write(self, w: Writer):
    scale = (self.p.flat_width_factor * self.p.note_spacing_y) / 6.0
    w.writeline('  <g transform="translate(%.2f %.2f) scale(%.2f %.2f)">' % (self.x, self.y, scale, scale))
    w.writeline(_FLAT_SYMBOL)
    w.writeline('  </g>')

class SvgFlat(SvgObjects):

  def __init__(self, p: GraphicSettings, c: Tuple(float, float)):
    super().__init__()
    w = p.flat_width_factor * p.note_spacing_y
    h = p.flat_height_factor * p.note_spacing_y
    x, y = c
    x1 = x - w/2
    x2 = x + w/2
    y1 = y + w/2
    y2 = y - w/2
    y3 = (y1 - h)
    self.add(SvgLine((x1, y1), (x1, y3), width=2))
    self.add(SvgLine((x1, y1), (x2, y), width=2))
    self.add(SvgLine((x1, y2), (x2, y), width=2))

class SvgNote(SvgObjects):

  def __init__(self, p: GraphicSettings, scale: Scale, note_index: int, tone: Tone):
    super().__init__()
    self.p = p
    self.note_index = note_index

    staff_index = p.get_staff(note_index)
    line_index = scale.get_staff_line(tone)
    x0 = p.get_note_x(note_index)
    y0 = p.get_line_y(staff_index, line_index)
    self.add(SvgCirc((x0, y0), p.note_radius))

    # Note tails
    x = x0 + p.note_radius - p.note_tail_width * 0.5
    self.add(SvgLine((x, y0), (x, y0 - p.note_tail_length), width=p.note_tail_width))

    # Draw floating lines below/above to reach note
    if line_index <= -2 or line_index >= 10:
      x1 = x0 - 2 * p.note_radius
      x2 = x0 + 2 * p.note_radius
      if line_index <= -2:
        lis = [i for i in range(line_index, -1) if i % 2 == 0]
      if line_index >= 10:
        lis = [i for i in range(10, line_index + 1) if i % 2 == 0]
      for i in lis:
        y = p.get_line_y(staff_index, i)
        self.add(SvgLine((x1, y), (x2, y), width=p.staff_line_width))

class SvgStaff(SvgObjects):

  def __init__(self, p: GraphicSettings, index: int, scale: Scale):
    super().__init__()
    self.p = p
    self.scale = scale
    self.index = index

    # Horizontal staff lines
    for i in range(0, 10, 2):
      x1 = p.get_measure_x(0) - p.key_padding_x
      x2 = x1 + p.staff_width
      y = p.get_line_y(index, i)
      self.add(SvgLine( (x1, y), (x2, y), width=p.staff_line_width))

    # Vertical measure separators
    for i in range(p.measures_per_line + 1):
      x = p.get_measure_x(i)
      y1 = p.get_line_y(index,  0) - p.measure_sep_offset_y
      y2 = p.get_line_y(index, 8) + p.measure_sep_offset_y
      self.add(SvgLine( (x, y1), (x, y2), width=p.measure_sep_width))

    # Key signiture (A3 -> G4)
    count = 0
    is_sharp, key_sig_lines = scale.get_key_signature()
    for i, staff_line in enumerate(key_sig_lines):
      print(staff_line)
      y = p.get_line_y(index, staff_line)
      x = p.get_key_sig_x(i)
      if scale.is_sharp_key():
        self.add(SvgSharp(p, (x, y)))
      else:
        self.add(SvgFlat2(p, x, y))
