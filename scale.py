from typing import List, Set, Tuple

from tone import *

"""
Keys where notes are written with sharps
"""
_SHARP_KEYS = [0, 7, 2, 9, 4, 11]

"""
Keys where notes are written with flats
"""
_FLAT_KEYS = [5, 10, 3, 8, 1, 6]

"""
Map from major key to list of notes which are sharp/flat
"""
_KEY_TO_KEY_SIG = {
  0: [],
  7: [5],
  2: [5, 0],
  9: [5, 0, 7],
  4: [5, 0, 7, 2],
  11: [5, 0, 7, 2, 9],
  5: [11],
  10: [11, 4],
  3: [11, 4, 9],
  8: [11, 4, 9, 2],
  1: [11, 4, 9, 2, 7],
  6: [11, 4, 9, 2, 7, 0],
}

KEY_LABELS = ['C', 'D-flat', 'D', 'D-sharp', 'E', 'F', 'G-flat', 'G', 'A-flat', 'A', 'B-flat', 'B']

"""
Which line of staff each note is printed on where
  0 = middle of top line (F)
  10 = middle of bottom line (E)
"""
_NOTE_TO_STAFF_INDEX = [10, 10, 9, 9, 8, 7, 7, 6, 6, 5, 5, 4]


class Scale():
  """Root note of scale in 0-12 form"""
  root: int
  """Root note of relative major scale in 0-12 form"""
  major_root: int
  mode: Mode
  note_set: Set[int]
  notes: np.ndarray

  def __init__(self, root: int, mode=Mode.MAJOR):
    self.root = root
    self.notes = np.array([(root + offset) % 12 for offset in mode.intervals])
    self.note_set = set(self.notes)
    self.mode = mode
    self.major_root = mode.root_to_major_root(root)

  def __str__(self) -> str:
    return '%s-%s' % (KEY_LABELS[self.root], str(self.mode))

  def __contains__(self, tone: Tone) -> bool:
    return tone.note in self.note_set

  def get_key_signature(self) -> Tuple[bool, List[int]]:
    sharp = self.is_sharp_key()
    return (sharp, [_NOTE_TO_STAFF_INDEX[n] for n in _KEY_TO_KEY_SIG[self.major_root]])

  def is_sharp_key(self) -> bool:
    return self.major_root in _SHARP_KEYS

  def get_staff_line(self, tone: Tone) -> int:
    """
    Gets index on staff for this tone in this scale.
    """
    if not tone in self:
      raise Exception('Tone %s not in scale %s' % (str(tone), str(self)))
    line = _NOTE_TO_STAFF_INDEX[tone.note] + (3 - tone.octave) * 7
    if not self.is_sharp_key() and tone.is_sharp():
      return line - 1
    return line

  def get_tones(self, start: Tone, end: Tone) -> List[Tone]:
    for tone in ToneRange(start, end):
      if tone in self:
        yield tone

  def get_tones2(self, tone_range: ToneRange) -> List[Tone]:
    for tone in tone_range:
      if tone in self:
        yield tone

if __name__ == '__main__':
  for tone in Scale(A, mode=Mode.MINOR).get_tones(Tone(A, 2), Tone(A, 3)):
    print(tone)

