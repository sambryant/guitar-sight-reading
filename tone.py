from __future__ import annotations
from enum import Enum
from functools import total_ordering
from typing import List, Set, Tuple
import numpy as np

DIATONIC = np.array([0, 2, 4, 5, 7, 9, 11])
NOTE_LABELS = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
IS_SHARP = [False, True, False, True, False, False, True, False, True, False, True, False]
C = 0
CSH = 1
D = 2
DSH = 3
E = 4
F = 5
FSH = 6
G = 7
GSH = 8
A = 9
ASH = 10
B = 11

"""
An integer between 0-11 inclusive representing notes C, C#, ..., B without a specific octave.
"""
Note = int


@total_ordering
class Tone():
  """
  Represents a note at a specific octave.
  """
  note: Note
  octave: int

  def __init__(self, note: Note, octave: int) -> None:
    self.note = note
    self.octave = octave

  def is_sharp(self) -> bool:
    return IS_SHARP[self.note]

  def __eq__(self, other):
    return (self.octave, self.note) == (other.octave, other.note)

  def __ne__(self, other):
    return (self.octave, self.note) != (other.octave, other.note)

  def __lt__(self, other):
    return (self.octave, self.note) < (other.octave, other.note)

  def next(self) -> Tone:
    if self.note == 11:
      return Tone(0, self.octave + 1)
    else:
      return Tone(self.note + 1, self.octave)

  def __repr__(self):
    return '%s%d' % (NOTE_LABELS[self.note], self.octave)

class ToneRange():

  def __init__(self, start: Tone, end: Tone) -> None:
    self.start = start
    self.end = end

  def __iter__(self):
    self.current = self.start
    return self

  def __next__(self):
    if self.current <= self.end:
      note = self.current
      self.current = note.next()
      return note
    else:
      raise StopIteration

  def __contains__(self, tone: Tone) -> bool:
    return tone >= self.start and tone <= self.end

ToneRange.KeySignature = ToneRange(Tone(A, 3), Tone(GSH, 4))

class Mode(Enum):
  MAJOR = ('major', [0, 2, 4, 5, 7, 9, 11])
  MINOR = ('minor', [0, 2, 3, 5, 7, 8, 10])

  def __init__(self, label: str, intervals: List[int]) -> None:
    self.label = label
    self.intervals = intervals

  def __str__(self) -> str:
    return self.label

  def root_to_major_root(self, root: int) -> int:
    if self == Mode.MAJOR:
      return root
    else:
      return (root + 3) % 12


class Position(Enum):
  FIRST = (ToneRange(Tone(E, 2), Tone(GSH, 4)), '1st Position', 'p1')
  THIRD = (ToneRange(Tone(G, 2), Tone(B, 4)), '3rd Position', 'p3')
  FIFTH = (ToneRange(Tone(A, 2), Tone(CSH, 5)), '5th Position', 'p5')
  EIGTH = (ToneRange(Tone(C, 3), Tone(E, 5)), '8th Position', 'p8')

  def __init__(self, range: ToneRange, title: str, shortname: str) -> None:
    self.range = range
    self.title = title
    self.shortname = shortname

  def __str__(self) -> str:
    return self.title

  def get_fingering(self, tone: Tone) -> Fingering:
    if tone not in self.range:
      raise Exception('Tone %s not in position %s' % (tone, self))

    t = self.range.start
    for string, steps in enumerate([5, 5, 5, 4, 5, 5]):
      for step in range(steps):
        if t == tone:
          if self != Position.FIRST:
            return Fingering(string, min(step + 1, 4))
          else:
            return Fingering(string, step)
        t = t.next()
    raise Exception('Should never happen')

_STRINGS = ['E', 'A', 'D', 'G', 'B', 'e']
class Fingering():
  string: int # 0 = low E, 5 = high E
  finger: int # 0 = open, 4 = pinky

  def __init__(self, string, finger) -> None:
    self.string = string
    self.finger = finger

  def __str__(self) -> str:
    return '(%s, %d)' % (_STRINGS[self.string], self.finger)
