from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List, Set, Tuple, Optional
import os

class Writer(ABC):

  @abstractmethod
  def writeline(self, line: str) -> None:
    pass

  @abstractmethod
  def writelines(self, lines: List[str]) -> None:
    pass

class FileWriter(Writer):
  file: File

  def __init__(self, output_name):
    if not os.path.exists(os.path.dirname(output_name)):
      os.mkdir(os.path.dirname(output_name))
      print('Created directory %s' % os.path.dirname(output_name))
    self.file = open(output_name, 'w')
    print('Created file %s' % output_name)

  def writeline(self, line: str) -> None:
    self.file.write(line)
    self.file.write('\n')

  def writelines(self, lines: List[str]) -> None:
    self.file.write('\n'.join(lines) + '\n')

class StrWriter(Writer):
  _content: str

  def __init__(self):
    self._content = ''

  def writeline(self, line: str) -> None:
    self._content += line
    self._content += '\n'

  def writelines(self, lines: List[str]) -> None:
    self._content += '\n'.join(lines)
    self._content += '\n'

class IndentedWriter(StrWriter):
  _depth: int
  _indent: str

  def __init__(self, depth: int) -> None:
    super().__init__()
    self._depth = depth
    self._indent = '  ' * depth

  def writeline(self, line: str) -> None:
    self._content += '%s%s\n' % (self._indent, line)

  def writelines(self, lines: List[str]) -> None:
    self._content += self._indent + ('\n%s' % self._indent).join(lines) + '\n'

class HTMLHeader():

  def __init__(self):
    pass

  def write(self, f: File):
    f.write("""
<header>
  <style>
    svg {
      height: fit-content;
      max-width: 100%;
    }
  </style>
</header>
""")

class HTMLSection(IndentedWriter):
  parent: Optional[HTMLSection]
  id: str
  depth: int
  title: str
  children: List[HTMLSection]

  def __init__(self, parent: HTMLSection, depth: int, id: str, title: str) -> None:
    super().__init__(depth)
    self.parent = parent
    self.id = id
    self.title = title
    self.children = []
    self.writeline('<h%d id="%s">%s</h%d>' % (self._depth, self.id, self.title, self._depth))

  def write_toc_entry(self, f: File):
    if len(self.children) == 0:
      f.write('<li><a href="#%s">%s</a></li>\n' % (self.id, self.title))
    else:
      if self.parent is not None:
        f.write('<li><a href="#%s">%s</a>\n' % (self.id, self.title))
      else:
        f.write('<p>Contents</p>\n')
      f.write('<ul>')
      for c in self.children:
        c.write_toc_entry(f)
      f.write('</ul>')
      if self.parent is not None:
        f.write('</li>\n')

  def write_to_file(self, f: File, include_toc: bool = True):
    f.write(self._content)

    if include_toc:
      self.write_toc_entry(f)

    for child in self.children:
      f.write('%s<div>\n' % (self._indent))
      child.write_to_file(f, include_toc = False)
      f.write('%s</div>\n' % (self._indent))

  @classmethod
  def root(cls, title: str, depth: int = 1) -> HTMLSection:
    return HTMLSection(None, depth, 'root', title)

  def add_child(self, title: str) -> Section:
    ind = len(self.children) + 1
    sec = HTMLSection(self, self._depth + 1, '%s-%02d' % (self.id, ind), title)
    self.children.append(sec)
    return sec


class HTMLBuffer():
  """
  This is a terrible implmentation but performance is not a priority here.
  """
  buffer: str
  def __init__(self):
    self.buffer = ''

  def write(self, s: str):
    self.buffer += s
