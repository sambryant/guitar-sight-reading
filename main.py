from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List, Set, Tuple
import argparse
import numpy as np
import os
import platform
import random
import sys

from scale import *
from tone import *
from sequence import *
from graphics import *
from writer import *

def main():
  parser = argparse.ArgumentParser(description='Generate practice sheet music')
  parser.add_argument('key', type=int, help='integer giving key between 0-11 with 0=C, 1=C#, etc')
  parser.add_argument('--mode', dest='mode', choices=['minor', 'major'], default='major')
  parser.add_argument('--num-exercises', dest='num_exercises', default=8, help='number of exercises to generate for each position/sequence type')
  parser.add_argument('--output-dir', dest='output_dir', default='output', help='sets directory where output file is written to')
  parser.add_argument('--no-open', dest='open_browser', default=True, action='store_const', const=False, help='dont open newly generated file in browser')

  args = parser.parse_args()
  if args.mode == 'minor':
    mode = Mode.MINOR
  else:
    mode = Mode.MAJOR
  output = generate_practice_set_all(args.output_dir, args.key, num_exercises=args.num_exercises, mode=mode)
  if args.open_browser:
    open_html_file(output)

def generate_practice_set_all(output_dir: str, key: int, num_exercises: int = 8, mode: Mode = Mode.MAJOR, lines_per_exercise: int=1):
  settings = GraphicSettings.load('graphics.json')
  generators = [
    ('Scale', SequenceGeneratorScale(), 1),
    ('Easy Sequences', SequenceGeneratorGeneral.load('easy'), num_exercises),
    ('Medium Sequences', SequenceGeneratorGeneral.load('standard'), num_exercises),
    ('Hard Sequences', SequenceGeneratorGeneral.load('hard'), num_exercises),
    ('Random', SequenceGeneratorRandom(), num_exercises),
  ]

  num_notes = settings.notes_per_line * lines_per_exercise
  scale = Scale(key, mode=mode)
  output_name = os.path.join(output_dir, 'practice-set-%s.html' % str(scale))

  header = HTMLHeader()
  sec_root = HTMLSection.root('Sightreading Practice in Key of %s' % str(scale))
  for position in Position:
    sec_pos = sec_root.add_child(position.title)

    for label, generator, num_exercises in generators:
      sec = sec_pos.add_child(label)
      for _ in range(num_exercises):
        tones = generator.generate(scale, position.range, num_notes)
        Graphics(settings, scale, tones).write(sec)

  if not os.path.exists(os.path.dirname(output_name)):
    os.mkdir(os.path.dirname(output_name))
  f = open(output_name, 'w')
  f.write('<html>\n')
  header.write(f)
  f.write('<body>\n')
  sec_root.write_to_file(f)
  f.write('</body>\n</html')
  print('Wrote file %s' % output_name)
  return output_name

def open_html_file(fname: str):
  try:
    import webbrowser
    path = os.path.realpath(fname)
    url = 'file://%s' % path
    webbrowser.open(url, new=2)
  except ImportError:
    print('Open file "%s" in your browser to check it out' % fname)

if __name__ == '__main__':
  main()
