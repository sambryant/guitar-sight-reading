from __future__ import annotations
import sys
from abc import ABC, abstractmethod
from typing import List, Set, Tuple
import numpy as np
import json
import random

from scale import *
from tone import *


class SequenceGenerator(ABC):

  @abstractmethod
  def generate(self, scale: Scale, range: ToneRange, length: int) -> List[List[Tone]]:
    pass

class SequenceGeneratorGeneral(SequenceGenerator):
  # Named value of this setting set
  name: str;
  # Gives probability for each note in scale (from 0..6) to be first
  first_note_prob: np.ndarray # floats length 7
  # Gives probability of melodic line switching from ascending to descending
  direction_switch_prob: float
  # Gives list of possible jumps from current note value
  allowed_movements: np.ndarray # ints
  # Gives probability for each jump in `allowed_movements` while moving up
  movement_prob_asc: np.ndarray # floats
  # Gives probability for each jump in `allowed_movements` while moving down
  movement_prob_des: np.ndarray # floats

  # Gives probability for 1/2/3/4 notes
  chord_probs: np.ndarray
  chord2_intervals: List[int]
  chord2_probs: np.ndarray
  chord3_intervals: List[int]
  chord3_probs: np.ndarray

  def __init__(self, json_values) -> None:
    self.name = json_values['name']
    self.first_note_prob = np.array(json_values['first_note_prob'], dtype=float)
    self.direction_switch_prob = json_values['direction_switch_prob']
    self.allowed_movements = np.array(json_values['allowed_movements'], dtype=int)
    self.movement_prob_asc = np.array(json_values['movement_prob_asc'], dtype=float)
    self.movement_prob_des = np.array(json_values['movement_prob_des'], dtype=float)
    self.chord_probs = np.array(json_values['chord_probs'], dtype=float)
    self.chord2_intervals = json_values['chord2_intervals']
    self.chord2_probs = np.array(json_values['chord2_probs'], dtype=float)
    self.chord3_intervals = json_values['chord3_intervals']
    self.chord3_probs = np.array(json_values['chord3_probs'], dtype=float)
    self.chord_probs /= np.sum(self.chord_probs)
    self.chord2_probs /= np.sum(self.chord2_probs)
    self.chord3_probs /= np.sum(self.chord3_probs)

  @classmethod
  def load(cls, name='standard', file='sequence.json') -> SequenceGeneratorGeneral:
    json_values = json.loads(open(file).read())
    for seq in json_values['sequences']:
      if seq['name'] == name:
        return cls(seq)
    raise Exception('Unknown sequence name: %s' % name)

  def generate(self, scale: Scale, range: ToneRange, length: int) -> List[Tone]:
    return SequenceGeneral(self, scale, range, length).get_output()

class SequenceGeneral():
  scale: Scale
  tones: List[List[Tone]]
  length: int
  p: SequenceGeneratorGeneral

  _chord_counts: List[int]

  # Index in tones list of first occurance of root note
  _first_root_index: int
  # Current note in the sequence
  _rel_note: int
  # All notes occuring so far
  _rel_notes: List[int]
  # Current direction of melody
  _moving_up: bool
  # Minimum note in the given range relative to first appearance of root note
  _rel_min: int
  # Maximum note in the given range relative to first appearance of root note
  _rel_max: int

  def __init__(self, p: SequenceGeneratorGeneral, scale: Scale, range: ToneRange, length: int) -> None:
    self.p = p
    self.scale = scale
    self.range = range
    self.length = length
    self.tones = list(self.scale.get_tones2(self.range))

    self._chord_counts = [i for i,_ in enumerate(self.p.chord_probs)]
    self._first_root_index = None
    for i,tone in enumerate(self.tones):
      if tone.note == scale.root:
        self._first_root_index = i
    self._rel_min = - self._first_root_index
    self._rel_max = len(self.tones) - self._first_root_index - 1
    self._rel_note = None
    self._rel_notes = []
    self._moving_up = random.random() < 0.5

  def get_output(self) -> List[List[Tone]]:
    while len(self._rel_notes) < self.length:
      self._gen_next()
    return [
      [
        self.tones[rel + self._first_root_index]
        for rel in rel_notes
      ]
      for rel_notes in self._rel_notes
    ]

  def _gen_next(self):
    if len(self._rel_notes) == 0:
      self._rel_note = self._pick_start_note()
      self._moving_up = random.random() < 0.5
    else:
      self._next_direction()
      self._compute_next_probs()
      self._rel_note = np.random.choice(self._next_rel_notes, p=self.next_rel_probs)

    self._rel_notes.append(self._gen_chord())

  def _gen_chord(self):
    num = np.random.choice(self._chord_counts, p=self.p.chord_probs)
    if num == 0:
      return [self._rel_note]
    elif num == 1:
      chord_notes = [self._rel_note + d for d in self.p.chord2_intervals]
      chord_avail = [rn >= self._rel_min and rn <= self._rel_max for rn in chord_notes]
      chord_probs = chord_avail * self.p.chord2_probs
      chord_probs /= np.sum(chord_probs)
      chord_note = np.random.choice(chord_notes, p=chord_probs)
      return [self._rel_note, chord_note]
    else:
      raise Exception('NIY')

  def _pick_start_note(self) -> int:
    tones = list(range(self._rel_min, self._rel_max + 1))
    tone_pos = [n % 7 for n in tones]
    tone_probs = self.p.first_note_prob
    tone_probs = [tone_probs[pos] for pos in tone_pos]
    tone_probs /= np.sum(tone_probs)
    return np.random.choice(tones, p=tone_probs)

  def _next_direction(self):
    # Determine whether to change melodic directions
    prob_switch = self.p.direction_switch_prob
    if self._moving_up and self._rel_note== self._rel_max:
      prob_switch = 1.0
    elif not self._moving_up and self._rel_note == self._rel_min:
      prob_switch = 1.0
    if random.random() < prob_switch:
      self._moving_up = not self._moving_up

  def _compute_next_probs(self):
    # Determine relative position of next notes
    self._next_rel_notes = self._rel_note + self.p.allowed_movements

    # Determine which of these are in given range
    avail = [rn >= self._rel_min and rn <= self._rel_max for rn in self._next_rel_notes]

    # Compute their relative probabilities
    if self._moving_up:
      self.next_rel_probs = self.p.movement_prob_asc * avail
    else:
      self.next_rel_probs = self.p.movement_prob_des * avail
    self.next_rel_probs /= sum(self.next_rel_probs)


class SequenceGeneratorRandom(ABC):

  def generate(self, scale: Scale, rng: ToneRange, length: int) -> List[List[Tone]]:
    tones = list(scale.get_tones2(rng))
    return [ [np.random.choice(tones)] for _ in range(length)]

class SequenceGeneratorScale(ABC):

  def generate(self, scale: Scale, range: ToneRange, length: int) -> List[List[Tone]]:
    tones = list(scale.get_tones2(range))
    tones.sort()
    print(', '.join([str(t) for t in tones]))
    return [[t] for i,t in enumerate(tones) if i < length]
