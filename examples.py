from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List, Set, Tuple
import argparse
import sys
import numpy as np
import random
import os

from scale import *
from tone import *
from position import *
from sequence import *
from graphics import *
from writer import *

def generate_examples():
  settings = GraphicSettings.load('graphics.json')
  num_notes = settings.notes_per_line

  scale = Scale(C, mode=Mode.MAJOR)
  gen = SequenceGeneratorGeneral.load('easy')
  position = Position.FIRST
  output = 'examples/c-major-p1-easy.svg'
  tones = gen.generate(scale, position.range, num_notes)
  Graphics(settings, scale, tones).write(FileWriter(output))

  scale = Scale(A, mode=Mode.MAJOR)
  gen = SequenceGeneratorGeneral.load('standard')
  position = Position.FIFTH
  output = 'examples/a-major-p5-standard.svg'
  tones = gen.generate(scale, position.range, num_notes)
  Graphics(settings, scale, tones).write(FileWriter(output))

  scale = Scale(E, mode=Mode.MINOR)
  gen = SequenceGeneratorGeneral.load('hard')
  position = Position.EIGTH
  output = 'examples/e-minor-p8-difficult.svg'
  tones = gen.generate(scale, position.range, num_notes)
  Graphics(settings, scale, tones).write(FileWriter(output))

if __name__ == '__main__':
  generate_examples()
